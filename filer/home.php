<?php
session_start();
include_once 'database.php';

if(!isset($_SESSION['user']))
{
 header("Location: index.php");
}
$res=mysql_query("SELECT * FROM filer_data WHERE fid=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
$filername=$userRow['fname'];
$res4=mysql_query("SELECT `request_id`,`priority`,`uid`,`filename` FROM `file_requests` WHERE `newold`=1 AND `assignbit`=1 AND `processing`=0 AND `rejectbit`=0 AND `assigned_to`='$filername'");

?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Filer HomePage</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script language="javascript" src="js/jquery-1.2.6.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="js/jquery-1.11.3-jquery.min.js"></script>
		<script type="text/javascript" src="js/popup.js"></script>

    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Filer Panel</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $userRow['fname']; ?><i class="caret"></i></a>
								
                                <ul class="dropdown-menu">
                                    <li>
                                        <center><img src="<?php echo $userRow['avtarpath']; ?>" width="120px" height="120px" class="avatar img-circle img-thumbnail" alt="avatar" /></center>
                                    </li>
									<li class="divider"></li>
									<li>
                                        <a tabindex="-1" href="#" class="launchLink">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
									<script language="javascript">
									$(function(){
										$('a#logout').click(function(){
											if(confirm('Are you sure you want to logout')) {
												return true;
											}

											return false;
										});
									});
									</script>
                                        <a tabindex="-1" id="logout" href="logout.php?logout">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
						<!--Experiment-->
						<div class="bgCover">&nbsp;</div>
						<div class="overlayBox">
							<div class="overlayContent">
								<a href="#" class="closeLink">[close]&times;</a>
								<!--<h2>Hello World</h2>
								<p>Feel free to put in what ever content you wish to. Images can also be shown</p>-->
								 <object type="text/html" data="profile.php" width="800px" height="550px" style="overflow:auto;">
    </object>
							</div>
						</div>
						
<script language="javascript">
function showOverlayBox() {
	//if box is not set to open then don't do anything
	if( isOpen == false ) return;
	// set the properties of the overlay box, the left and top positions
	$('.overlayBox').css({
		display:'block',
		left:( $(window).width() - $('.overlayBox').width() )/2,
		top:( $(window).height() - $('.overlayBox').height() )/2 -20,
		position:'absolute'
	});
	// set the window background for the overlay. i.e the body becomes darker
	$('.bgCover').css({
		display:'block',
		width: $(window).width(),
		height:$(window).height(),
	});
}
function doOverlayOpen() {
	//set status to open
	isOpen = true;
	showOverlayBox();
	$('.bgCover').css({opacity:0}).animate( {opacity:0.5, backgroundColor:'#000'} );
	// dont follow the link : so return false.
	return false;
}
function doOverlayClose() {
	//set status to closed
	isOpen = false;
	$('.overlayBox').css( 'display', 'none' );
	// now animate the background to fade out to opacity 0
	// and then hide it after the animation is complete.
	$('.bgCover').animate( {opacity:0}, null, null, function() { $(this).hide(); } );
}
// if window is resized then reposition the overlay box
$(window).bind('resize',showOverlayBox);
// activate when the link with class launchLink is clicked
$('a.launchLink').click( doOverlayOpen );
// close it when closeLink is clicked
$('a.closeLink').click( doOverlayClose );
	

</script>
						
						<!--Experiment-->
                        <ul class="nav">
                            <li class="active">
                                <a href="#">Dashboard</a>
                            </li>
                            
                            
                        </ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#search">Search</a></li>
						</ul>
                    </div>
                    <!--/.nav-collapse -->
					<!--Search code-->
					
					<div id="search">
						<button type="button" class="close">X</button>
						<form id="searchform" method="post">
							<input type="search" id="searchbox" name="searchbox" value="" placeholder="type keyword(s) here" autocomplete="off" required/>
							<button type="submit" id="reg-form" class="btn btn-primary">Search</button>
							
						</form>
					</div>
					
					<script language="javascript">
					$(function () {
						$('a[href="#search"]').on('click', function(event) {
							event.preventDefault();
							$('#search').addClass('open');
							$('#search > form > input[type="search"]').focus();
						});
						
						$('#search, #search button.close').on('click keyup', function(event) {
							if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
								$(this).removeClass('open');
							}
						});
						
						
						//Do not include! This prevents the form from submitting for DEMO purposes only!
						$('#searchform').submit(function(event) {
							var data = $(this).serialize();
							//event.preventDefault();
							$.ajax({
							type: 'POST',
							url: 'search.php',
							data: data,
							success: function(data){
									$(".search_res").html(data);
									
								//$(".search_res").load('search.php'); 
							}
							});
							return false;
						})
						
					});
					</script>
					<!--End search-->
                </div>
            </div>
        </div>
		<script>
    function printValue(sliderID, textbox) {
        var x = document.getElementById(textbox);
        var y = document.getElementById(sliderID);
        x.value = y.value;
    }

    window.onload = function() {  printValue('slider2', 'progress'); }
	
	function callme()
	{
		var e = document.getElementById("requestid");
		var reqid = e.options[e.selectedIndex].value;
      
		if(reqid!=0)
		{
			var sdata="func=showRequestprogress&request=" + reqid;
           
			$.ajax({ 
							type: "POST", 
							url: "home.php", 
							data: sdata,  
							success: function(msg1){
											
											$("#per").html(msg1);
											printValue('slider2','progress');
										
											
							}
				   });

		}
		else
		{
			$("#per").html("&nbsp;");
		}

	}
</script>
<?php
function showRequestprogress($request){
	$progressqry=mysql_query("select * from `progress` where `req_id`=$request");
	$progressrs=mysql_fetch_array($progressqry);
	$v= $progressrs['ppercent'];
	$b="<script language=\"javascript\"> $(\".bar\").animate({width: \"$v%\"}, 2500 );";
	$b.="$(\"#slider2\").val($v); $(\"#slider2\").slider(\"refresh\");";
	//$b.="printValue('slider2','progress');";
	$b.="</script>";
	$b.="$v%";
	echo $b;
	
}
if(isset($_POST['func']))
	{
		$_POST['func']($_POST['request']);
	}
	else
	{	
?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li class="active">
                            <a href="index.php"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="settings.php"><i class="icon-chevron-right"></i>Settings</a>
                        </li>
                    </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    <div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Welcome</h4>
                        	<sub><?php echo $userRow['email']; ?></sub></div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <form id="progressform" method="post">
	                                <ul class="breadcrumb">
	                                    
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
										<label>
											<select name="requestid" id="requestid" onChange="callme();">
											<option value='0'>--Select Request ID--</option>
											<?php 
											$qry=mysql_query("SELECT `req_id`,`ppercent` FROM `progress`");
											while($rs = mysql_fetch_row($qry))
											{
													  echo "<option value='$rs[0]'>$rs[0]</option>";
											}
											?>
											</select>
										  </label>
										</li>
										<li>Update progress:<input id="slider2" type="range" min="0" max="100" step="1" value="0" onchange="printValue('slider2','progress')">
									    </li><input type="hidden" name="filern_ame" value="<?php echo $filername; ?>" >
										<li><input id="progress" type="text" name="newprogval" style="height:20px;width:30px; line-height:20px;"></li>
										<li>
										<button type="submit" id="progress-submit" class="btn btn-primary">update</button>
										</li>
	                                    </form>
										<li><div class="updatemsg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
										
									<li><?php //code for file upload
									if(isset($_FILES['_file'])){
									$errors= array();
									  $file_name = $_FILES['_file']['name'];
									  $file_size = $_FILES['_file']['size'];
									  $file_tmp = $_FILES['_file']['tmp_name'];
									  $file_type = $_FILES['_file']['type'];
									  $file_ext=strtolower(end(explode('.',$_FILES['_file']['name'])));
									  
									  $expensions= array("jpeg","jpg","png","pdf","docx","xlsx",'zip','txt','html','gz','mp3','gif','mpg','mov','phps','doc');
									  
									  if(in_array($file_ext,$expensions)=== false){
										 $errors[]="extension not allowed, please choose a JPEG or PNG file.";
									  }
									  
									  if($file_size > 20971520) {
										 $errors[]='File size must be excately 20 MB';
									  }
									  
									  if(empty($errors)==true) {
										 move_uploaded_file($file_tmp,"../files/".$file_name);
										 echo "Success";
									  }else{
										 print_r($errors);
									  }
									}
									//end code for file upload 
									//mysql_query("INSERT INTO `activites`(`pop`,`uid`, `cancel_reqid`) VALUES ('file Updated By $filername' ,$uid,$reqid)");
									?>
									
									</li>
	                                </ul>
									
                            	</div>
                        	</div>
                    	</div>
						<script language="javascript">
					$(function () {
						
						$('#progressform').submit(function(event) {
							var data = $(this).serialize();
							//event.preventDefault();
							$.ajax({
							type: 'POST',
							url: 'progupdate.php',
							data: data,
							success: function(data){
									$(".updatemsg").html(data);
									
								//$(".search_res").load('search.php'); 
							}
							});
							return false;
						})
						
					});
					</script>
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Progress of request</div>
                                
                            </div>
                            <div class="block-content collapse in">
                                
								<div class="progress progress-striped progress-success active">
											<div style="width: 0%;" class="bar"></div>
										</div><div id="per"></div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Request</div>
                                    <div class="pull-right">

                                    </div>
                                </div>
                                <div class="block-content collapse in"></br></br>
								<ul id = "myTab" class = "nav nav-tabs">
								   <li class = "active">
									  <a href = "#Reqdetails" data-toggle = "tab">Request Details</a></li>
								   <li><a href = "#completedreq" data-toggle = "tab">Completed Requests</a></li>
								   <li><a href = "#pendingreq" data-toggle = "tab">Pending Requests</a></li>
								   
								</ul>
                                </div>
								<script language="javascript">
					$(function () {
						$('a[href="#requesttab"]').on('click', function(event) {
							event.preventDefault();
							$('#requesttab').addClass('open');
							$('#requesttab > form > input[type="search"]').focus();
						});
						$('#reqform').submit(function(event) {
							var data = $(this).serialize();
							//event.preventDefault();
							$.ajax({
							type: 'POST',
							url: 'notify.php',
							data: data,
							success: function(data){
									$(".req_d").html(data);
									
								//$(".search_res").load('search.php'); 
							}
							});
							return false;
						})
						
					});
					</script>
								<div id = "myTabContent" class = "tab-content">

						   <div class = "tab-pane fade in active" id = "Reqdetails">
							  <div id="requesttab" style="padding-left:20px;">
								&nbsp;&nbsp;
								<form id="reqform" method="post">
									<input type="search" id="reqbox" name="reqbox" value="" style="height: 33.818182px;width: 201.818182px;line-height:30px; " required/>
									<button type="submit" id="c_req" class="btn btn-primary">populate</button>
									<div class="reqres"></div>
								</form>
							</div>
							<div class="req_d"></div>
						   </div>
						   
						   <div class = "tab-pane fade" id = "completedreq">
						   <?php
							$query=mysql_query("SELECT * FROM `file_requests` WHERE `status`='completed' AND `assigned_to`='$filername'");
							//$userRow=mysql_fetch_array($query);
							$p=mysql_num_rows($query);
							?>
							  <p><?php 
								//echo $p;
								if($p==0){echo "No files completed!"; }
								else{
									echo "<table border=1>";
									echo "<th>Request_id</th><th>Requestor</th><th>type</th><th>Priority</th><th>category</th><th>Filename</th><th>Created on</th><th>Completed on</th><th>Status</th>";
									while($row = mysql_fetch_row($query)) {
									echo "<tr>";
										
										echo "<td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td><td>$row[8]</td><td>$row[9]</td><td>$row[10]</td><td>$row[16]</td>";
									
									}

								echo "<tr></table>";
								//echo $userRow['fname'].",".$userRow['email'];
								 }?></p>
						   </div>
						   <div class = "tab-pane fade" id = "pendingreq" style="overflow:scroll; height:400px;">
							  
							  <?php
							$query1=mysql_query("SELECT * FROM `file_requests` WHERE `status`='Pending' AND `assigned_to`='$filername'");
							//$userRow=mysql_fetch_array($query);
							$q=mysql_num_rows($query1);
							?>
							  <p><?php 
								//echo $p;
								if($q==0){echo "No files Pending!"; }
								else{
									echo "<table border=1>";
									echo "<th>Request_id</th><th>Requestor</th><th>type</th><th>Priority</th><th>category</th><th>Filename</th><th>Created on</th><th>Status</th>";
									while($row = mysql_fetch_row($query1)) {
									echo "<tr>";
										
										echo "<td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td><td>$row[8]</td><td>$row[9]</td><td>$row[16]</td>";
									
									}

								echo "<tr></table>";
								//echo $userRow['fname'].",".$userRow['email'];
								 }?></p>
							  
						   </div>
						   <div class = "tab-pane fade in" id = "cancelreq">
							  <?php
							$query2=mysql_query("SELECT * FROM `file_requests` WHERE `cancelbit`=1");
							//$userRow=mysql_fetch_array($query);
							$y=mysql_num_rows($query2);
							?>
							  <p><?php 
								//echo $p;
								if($y==0){echo "No cancellation requests!"; }
								else{
									echo "<table border=1>";
									echo "<th>Request_id</th><th>Requestor</th><th>type</th><th>Priority</th><th>category</th><th>Filename</th><th>Created on</th><th>Status</th><th>Link</th>";
									while($row2 = mysql_fetch_row($query2)) {
									echo "<tr>";
										$str="<td><form method=\"post\" action=\"cancel.php\" target=\"_blank\"><input type=\"hidden\" name=\"reqid_n\" value=\"$row2[1]\" /><button type=\"submit\" class=\"btn btn-link\">Link</button></form></td>";
										echo "<td>$row2[1]</td><td>$row2[2]</td><td>$row2[3]</td><td>$row2[4]</td><td>$row2[5]</td><td>$row2[8]</td><td>$row2[9]</td><td>$row2[11]</td>".$str;
										
									
									}

								echo "<tr></table>";
								//echo $userRow['fname'].",".$userRow['email'];
								 }?></p>
							<div class="req_d"></div>
						   </div>
						   
						  </div>
                            </div>
                            <!-- /block -->
							<!--tab content-->
							
							<!--tab content-->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Notifications</div>
                                </div>
                                <div class="block-content collapse in">
                                    <form action = "" method = "POST" enctype = "multipart/form-data">
									 Upload file:  <input type = "file" name = "_file" />
									 <input type = "submit" class="btn btn-success" value="Upload"/>
										
								  </form>
                                 <table class="table table-striped" >
                                        
                                        <thead>
                                            <tr>
                                                <th>Request ID</th>
                                                <th>Priority</th>
                                                <th>Assign Filer</th>
												<th>Download</th>
												<th>View Request</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
											//$filername=$userRow['fname'];
												//$res4=null;								
										while($rowset = mysql_fetch_row($res4)){
											echo "<tr>";
											echo "<td>".$rowset[0]."</td>";
											echo "<td>".$rowset[1]."</td>";
											$strhidden="<input type=\"hidden\" name=\"filername\" value=\"$filername\" />";
											echo "<td><form method=\"post\" action=\"approve.php\"><input type=\"hidden\" name=\"reqid_new\" value=\"$rowset[0]\" /><input type=\"hidden\" name=\"uid\" value=\"$rowset[2]\" />$strhidden<button type=\"submit\" name=\"accept\" value=\"Assign\" class=\"btn btn-success\">Accept</button>&nbsp;&nbsp;	<button type=\"submit\" name=\"reject\" value=\"Assign\" class=\"btn btn-danger\">Reject</button></form></td><td><a href=\"../files/$rowset[3]\">DOWNLOAD</a></td>";
											echo "<form method=\"post\" action=\"view.php\"><input type=\"hidden\" name=\"id_req\" value=\"$rowset[0]\" /><input type=\"hidden\" name=\"id_u\" value=\"$rowset[2]\" /><td><button type=\"submit\" name=\"view\" value=\"Assign\" class=\"btn btn-success\">VIEW</button></form></td>";
											echo "</tr>";
										}
										?>
                                        </tbody>
                                    </table>
                                   
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Search Results</div>
                                    
                                </div>
                                <div class="block-content collapse in">
                                    <div class="search_res"></div>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                        <div class="span6">
                            <!-- block -->
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">Recently Uploaded files</div>
                                </div>
                                <div class="block-content collapse in">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Req ID</th>
                                                <th>Date & time</th>
                                                <th>Filename</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>02/02/2013</td>
                                                <td>doc1</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>01/02/2013</td>
                                                <td>doc2</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>01/02/2013</td>
                                                <td>doc3</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /block -->
                        </div>
                    </div>
                    <div class="row-fluid">
                        <!-- block -->
                        
                        <!-- /block -->
                    </div>
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; </p>
            </footer>
        </div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
	<?php } ?>
    </body>

</html>