-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2016 at 05:45 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oes`
--

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `organization_name` varchar(255) NOT NULL,
  `domain_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `author` varchar(255) NOT NULL,
  `sms` tinyint(1) NOT NULL,
  `email_notification` tinyint(1) NOT NULL,
  `guest_login` tinyint(1) NOT NULL,
  `front_end` tinyint(1) NOT NULL,
  `slides` tinyint(4) NOT NULL,
  `translate` tinyint(4) NOT NULL DEFAULT '0',
  `paid_exam` tinyint(4) NOT NULL DEFAULT '1',
  `leader_board` tinyint(1) NOT NULL DEFAULT '1',
  `contact` text NOT NULL,
  `currency` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`id`, `name`, `organization_name`, `domain_name`, `email`, `meta_title`, `meta_desc`, `timezone`, `author`, `sms`, `email_notification`, `guest_login`, `front_end`, `slides`, `translate`, `paid_exam`, `leader_board`, `contact`, `currency`, `photo`, `created`, `modified`) VALUES
(1, 'Mocktiment', 'Park cars', 'mocktiment.com', 'parkgroup1234@gmail.com', 'Oes', 'Oes', 'Asia/Kolkata', 'Exam Solution', 0, 0, 0, 0, 1, 0, 0, 1, '0000-0000~info@eduexpression.com~http://facebook.com', 'fa fa-usd', '', '2014-04-08 20:56:04', '2016-01-07 04:47:43');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL,
  `link_name` varchar(255) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `is_url` varchar(8) NOT NULL DEFAULT 'Internal',
  `url` varchar(255) NOT NULL,
  `url_target` varchar(6) NOT NULL,
  `main_content` longtext NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `cols` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '1',
  `published` varchar(11) NOT NULL DEFAULT 'Published',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diffs`
--

CREATE TABLE IF NOT EXISTS `diffs` (
`id` int(11) NOT NULL,
  `diff_level` varchar(15) NOT NULL,
  `type` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diffs`
--

INSERT INTO `diffs` (`id`, `diff_level`, `type`) VALUES
(1, 'Easy', 'E'),
(2, 'Medium', 'M'),
(3, 'Difficult', 'D');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE IF NOT EXISTS `exams` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `instruction` text NOT NULL,
  `duration` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `passing_percent` int(11) NOT NULL,
  `negative_marking` varchar(3) NOT NULL,
  `attempt_count` int(11) NOT NULL,
  `declare_result` varchar(3) NOT NULL DEFAULT 'Yes',
  `finish_result` char(1) NOT NULL DEFAULT '0',
  `ques_random` char(1) NOT NULL DEFAULT '0',
  `paid_exam` char(1) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Inactive',
  `type` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `finalized_time` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `name`, `instruction`, `duration`, `start_date`, `end_date`, `passing_percent`, `negative_marking`, `attempt_count`, `declare_result`, `finish_result`, `ques_random`, `paid_exam`, `amount`, `status`, `type`, `user_id`, `finalized_time`, `created`, `modified`) VALUES
(1, 'Demo Exam', 'Please read Instruction', 120, '2014-12-15 14:36:07', '2016-01-03 14:36:00', 50, 'Yes', 9, 'Yes', '1', '0', '0', NULL, 'Active', 'Exam', 0, NULL, '2014-12-19 14:36:38', '2016-01-02 13:47:07'),
(2, 'exam', 'dfgdfhfd', 50, '2016-01-02 13:58:00', '2016-01-05 13:58:00', 25, 'Yes', 2, 'Yes', '1', '0', '0', NULL, 'Active', 'Exam', 0, NULL, '2016-01-02 13:58:45', '2016-01-02 14:00:48'),
(5, 'practice3', 'fgggv', 10, '2016-01-03 18:10:00', '2016-02-05 18:10:00', 20, 'Yes', 3, 'Yes', '1', '0', '0', NULL, 'Active', 'Prepration', 0, NULL, '2016-01-03 18:12:08', '2016-01-05 15:38:57'),
(6, 'exam1', 'all the best', 10, '2016-02-04 11:34:00', '2016-03-04 11:35:00', 40, 'No', 6, 'Yes', '1', '0', '0', NULL, 'Active', 'Exam', 0, NULL, '2016-02-04 11:36:14', '2016-02-04 11:43:23'),
(7, 'exam2', 'all the best good luck.', 15, '2016-02-04 11:36:00', '2016-03-11 11:37:00', 35, 'Yes', 7, 'Yes', '0', '1', '0', NULL, 'Active', 'Prepration', 0, NULL, '2016-02-04 11:38:43', '2016-02-04 11:46:01'),
(8, 'ep3', 'good luck', 4, '2016-02-03 12:07:00', '2016-04-08 12:07:00', 0, 'Yes', 5, 'Yes', '1', '0', '0', NULL, 'Active', 'Prepration', 0, NULL, '2016-02-04 12:10:14', '2016-02-04 12:12:08'),
(9, 'new exam 2 march', 'hey hi', 5, '2016-03-02 14:10:00', '2016-03-17 14:10:00', 50, 'No', 1, 'Yes', '1', '0', '0', NULL, 'Active', 'Exam', 0, NULL, '2016-03-02 14:10:56', '2016-03-02 14:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `exam_groups`
--

CREATE TABLE IF NOT EXISTS `exam_groups` (
`id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_groups`
--

INSERT INTO `exam_groups` (`id`, `exam_id`, `group_id`) VALUES
(6, 1, 2),
(7, 1, 1),
(8, 1, 3),
(9, 2, 2),
(10, 2, 1),
(11, 2, 3),
(24, 5, 2),
(25, 5, 1),
(26, 5, 3),
(27, 6, 2),
(28, 6, 1),
(29, 6, 3),
(33, 7, 2),
(34, 7, 1),
(35, 7, 3),
(39, 8, 2),
(40, 8, 1),
(41, 8, 3),
(42, 9, 2),
(43, 9, 1),
(44, 9, 3);

-- --------------------------------------------------------

--
-- Table structure for table `exam_orders`
--

CREATE TABLE IF NOT EXISTS `exam_orders` (
`id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_preps`
--

CREATE TABLE IF NOT EXISTS `exam_preps` (
`id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `ques_no` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_preps`
--

INSERT INTO `exam_preps` (`id`, `exam_id`, `subject_id`, `ques_no`, `type`, `level`) VALUES
(3, 5, 3, 5, '1', '1,2,3'),
(4, 7, 3, 7, '1,2,3,4', '1,2,3'),
(5, 8, 3, 1, '1', '1,2,3');

-- --------------------------------------------------------

--
-- Table structure for table `exam_questions`
--

CREATE TABLE IF NOT EXISTS `exam_questions` (
`id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_questions`
--

INSERT INTO `exam_questions` (`id`, `exam_id`, `question_id`) VALUES
(1, 1, 3),
(2, 1, 2),
(3, 1, 1),
(4, 2, 15),
(5, 2, 14),
(6, 2, 13),
(7, 2, 12),
(8, 2, 11),
(9, 2, 10),
(10, 2, 9),
(11, 2, 8),
(12, 2, 7),
(13, 2, 6),
(14, 2, 3),
(15, 2, 1),
(16, 6, 12),
(17, 6, 11),
(18, 6, 9),
(19, 6, 10),
(21, 6, 7),
(22, 6, 6),
(23, 6, 14),
(24, 6, 13),
(25, 6, 8),
(26, 9, 16),
(27, 9, 15),
(28, 9, 14),
(29, 9, 13),
(30, 9, 12),
(31, 9, 11);

-- --------------------------------------------------------

--
-- Table structure for table `exam_results`
--

CREATE TABLE IF NOT EXISTS `exam_results` (
`id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `total_question` int(11) NOT NULL,
  `total_answered` int(11) NOT NULL,
  `total_marks` int(11) NOT NULL,
  `obtained_marks` decimal(18,2) NOT NULL,
  `result` varchar(10) NOT NULL,
  `percent` decimal(5,2) NOT NULL,
  `finalized_time` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_results`
--

INSERT INTO `exam_results` (`id`, `exam_id`, `student_id`, `start_time`, `end_time`, `total_question`, `total_answered`, `total_marks`, `obtained_marks`, `result`, `percent`, `finalized_time`, `user_id`) VALUES
(1, 1, 3, '2016-01-02 13:55:07', '2016-01-02 14:01:21', 3, 0, 12, '0.00', 'Fail', '0.00', '2016-01-02 14:01:21', 1),
(2, 2, 3, '2016-01-02 14:02:13', '2016-01-02 14:05:00', 12, 12, 40, '17.00', 'Pass', '42.50', '2016-01-02 14:05:00', 1),
(4, 5, 5, '2016-01-03 18:12:35', '2016-01-03 18:15:53', 0, 5, 19, '2.00', 'Fail', '-10.53', '2016-01-03 18:15:53', 1),
(5, 2, 5, '2016-01-03 18:24:05', '2016-01-03 18:25:38', 12, 10, 40, '16.00', 'Pass', '40.00', '2016-01-03 18:25:38', 1),
(6, 2, 6, '2016-01-03 18:47:23', '2016-01-03 18:49:22', 12, 12, 40, '17.00', 'Pass', '42.50', '2016-01-03 18:49:22', 1),
(7, 2, 4, '2016-01-03 21:40:43', '2016-01-03 21:44:26', 12, 12, 40, '5.00', 'Fail', '12.50', '2016-01-03 21:44:26', 1),
(8, 5, 4, '2016-01-03 21:46:04', '2016-01-03 21:47:10', 0, 5, 20, '7.00', 'Pass', '35.00', '2016-01-03 21:47:10', 1),
(9, 5, 5, '2016-01-05 15:39:06', '2016-01-27 10:42:59', 0, 0, 17, '5.00', 'Fail', '0.00', '2016-01-27 10:42:59', 1),
(10, 5, 4, '2016-01-05 23:44:22', '2016-01-05 23:45:02', 0, 4, 16, '4.00', 'Pass', '25.00', '2016-01-05 23:45:02', 1),
(11, 5, 4, '2016-01-27 10:38:09', '2016-01-27 10:38:26', 0, 0, 15, '0.00', 'Fail', '0.00', '2016-01-27 10:38:26', 1),
(12, 5, 5, '2016-01-27 10:43:10', '2016-01-27 10:46:45', 0, 0, 20, '0.00', 'Fail', '0.00', '2016-01-27 10:46:45', 1),
(13, 5, 1, '2016-02-04 00:30:34', '2016-02-04 00:31:24', 0, 5, 18, '8.00', 'Pass', '44.44', '2016-02-04 00:31:24', 1),
(14, 5, 1, '2016-02-04 00:41:05', '2016-02-04 00:41:45', 0, 5, 20, '2.00', 'Fail', '10.00', '2016-02-04 00:41:45', 1),
(15, 5, 1, '2016-02-04 11:28:31', '2016-02-04 11:29:18', 0, 5, 19, '16.00', 'Pass', '84.21', '2016-02-04 11:29:18', 1),
(16, 6, 1, '2016-02-04 11:46:46', '2016-02-04 11:49:06', 9, 9, 31, '10.00', 'Fail', '32.26', '2016-02-04 11:49:06', 1),
(17, 7, 1, '2016-02-04 11:55:28', '2016-02-04 11:56:42', 0, 7, 26, '20.00', 'Pass', '76.92', '2016-02-04 11:57:30', 1),
(18, 7, 1, '2016-02-04 12:00:24', '2016-02-04 12:01:18', 0, 7, 24, '0.00', '', '0.00', NULL, 0),
(19, 8, 1, '2016-02-04 12:12:29', '2016-02-04 12:12:45', 0, 1, 4, '4.00', 'Pass', '100.00', '2016-02-04 12:12:45', 1),
(20, 8, 1, '2016-02-04 12:13:12', '2016-02-04 12:13:25', 0, 1, 4, '-1.00', 'Fail', '-25.00', '2016-02-04 12:13:25', 1),
(21, 7, 4, '2016-02-25 15:28:36', '2016-03-02 14:12:43', 0, 0, 27, '0.00', '', '0.00', NULL, 0),
(22, 9, 4, '2016-03-02 14:12:59', '2016-03-02 14:14:41', 6, 5, 18, '8.00', 'Fail', '44.44', '2016-03-02 14:14:41', 1),
(23, 8, 4, '2016-03-23 12:31:02', '2016-03-23 13:00:30', 0, 0, 4, '0.00', 'Pass', '0.00', '2016-03-23 13:00:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam_stats`
--

CREATE TABLE IF NOT EXISTS `exam_stats` (
`id` int(11) NOT NULL,
  `exam_result_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `ques_no` int(11) NOT NULL,
  `attempt_time` datetime DEFAULT NULL,
  `opened` char(1) NOT NULL DEFAULT '0',
  `answered` char(1) NOT NULL DEFAULT '0',
  `review` char(1) NOT NULL DEFAULT '0',
  `option_selected` varchar(15) NOT NULL,
  `answer` text NOT NULL,
  `true_false` varchar(5) NOT NULL,
  `fill_blank` varchar(100) NOT NULL,
  `marks` int(11) NOT NULL,
  `marks_obtained` decimal(5,2) NOT NULL,
  `closed` char(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `checking_time` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_stats`
--

INSERT INTO `exam_stats` (`id`, `exam_result_id`, `exam_id`, `student_id`, `question_id`, `ques_no`, `attempt_time`, `opened`, `answered`, `review`, `option_selected`, `answer`, `true_false`, `fill_blank`, `marks`, `marks_obtained`, `closed`, `user_id`, `checking_time`) VALUES
(1, 1, 1, 3, 3, 1, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(2, 1, 1, 3, 2, 2, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(3, 1, 1, 3, 1, 3, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(4, 2, 2, 3, 3, 1, '2016-01-02 14:02:38', '1', '1', '0', '1', '', '', '', 4, '0.00', '1', 0, NULL),
(5, 2, 2, 3, 6, 2, '2016-01-02 14:03:02', '1', '1', '0', '4', '', '', '', 4, '0.00', '1', 0, NULL),
(6, 2, 2, 3, 7, 3, '2016-01-02 14:03:09', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(7, 2, 2, 3, 8, 4, '2016-01-02 14:03:16', '1', '1', '0', '2', '', '', '', 2, '0.00', '1', 0, NULL),
(8, 2, 2, 3, 9, 5, '2016-01-02 14:03:24', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(9, 2, 2, 3, 10, 6, '2016-01-02 14:03:30', '1', '1', '0', '2', '', '', '', 4, '-1.00', '1', 0, NULL),
(10, 2, 2, 3, 11, 7, '2016-01-02 14:03:40', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(11, 2, 2, 3, 12, 8, '2016-01-02 14:04:11', '1', '1', '1', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(12, 2, 2, 3, 13, 9, '2016-01-02 14:04:30', '1', '1', '0', '3', '', '', '', 2, '-1.00', '1', 0, NULL),
(13, 2, 2, 3, 14, 10, '2016-01-02 14:04:34', '1', '1', '0', '4', '', '', '', 3, '3.00', '1', 0, NULL),
(14, 2, 2, 3, 1, 11, '2016-01-02 14:04:40', '1', '1', '0', '', '', '', '', 4, '-1.00', '1', 0, NULL),
(15, 2, 2, 3, 15, 12, '2016-01-02 14:04:50', '1', '1', '0', '', '', 'False', '', 1, '1.00', '1', 0, NULL),
(18, 4, 5, 5, 6, 1, '2016-01-03 18:12:45', '1', '1', '0', '1', '', '', '', 4, '0.00', '1', 0, NULL),
(19, 4, 5, 5, 5, 2, '2016-01-03 18:13:09', '1', '1', '0', '4', '', '', '', 4, '-1.00', '1', 0, NULL),
(20, 4, 5, 5, 4, 3, '2016-01-03 18:13:49', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(21, 4, 5, 5, 11, 4, '2016-01-03 18:15:41', '1', '1', '1', '1', '', '', '', 4, '0.00', '1', 0, NULL),
(22, 4, 5, 5, 14, 5, '2016-01-03 18:14:20', '1', '1', '0', '2', '', '', '', 3, '0.00', '1', 0, NULL),
(23, 5, 2, 5, 3, 1, '2016-01-03 18:24:15', '1', '1', '0', '4', '', '', '', 4, '0.00', '1', 0, NULL),
(24, 5, 2, 5, 6, 2, '2016-01-03 18:24:20', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(25, 5, 2, 5, 7, 3, '2016-01-03 18:24:26', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(26, 5, 2, 5, 8, 4, '2016-01-03 18:24:32', '1', '1', '0', '5', '', '', '', 2, '0.00', '1', 0, NULL),
(27, 5, 2, 5, 9, 5, '2016-01-03 18:24:38', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(28, 5, 2, 5, 10, 6, '2016-01-03 18:24:43', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(29, 5, 2, 5, 11, 7, '2016-01-03 18:24:56', '1', '1', '0', '3', '', '', '', 4, '0.00', '1', 0, NULL),
(30, 5, 2, 5, 12, 8, '2016-01-03 18:25:01', '1', '1', '0', '1', '', '', '', 4, '-1.00', '1', 0, NULL),
(31, 5, 2, 5, 13, 9, '2016-01-03 18:25:09', '1', '1', '0', '2', '', '', '', 2, '2.00', '1', 0, NULL),
(32, 5, 2, 5, 14, 10, '2016-01-03 18:25:14', '1', '1', '0', '4', '', '', '', 3, '3.00', '1', 0, NULL),
(33, 5, 2, 5, 1, 11, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(34, 5, 2, 5, 15, 12, NULL, '1', '0', '0', '', '', '', '', 1, '0.00', '1', 0, NULL),
(35, 6, 2, 6, 3, 1, '2016-01-03 18:47:31', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(36, 6, 2, 6, 6, 2, '2016-01-03 18:47:38', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(37, 6, 2, 6, 7, 3, '2016-01-03 18:47:44', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(38, 6, 2, 6, 8, 4, '2016-01-03 18:47:53', '1', '1', '0', '1', '', '', '', 2, '2.00', '1', 0, NULL),
(39, 6, 2, 6, 9, 5, '2016-01-03 18:47:59', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(40, 6, 2, 6, 10, 6, '2016-01-03 18:48:27', '1', '1', '0', '1', '', '', '', 4, '-1.00', '1', 0, NULL),
(41, 6, 2, 6, 11, 7, '2016-01-03 18:48:36', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(42, 6, 2, 6, 12, 8, '2016-01-03 18:48:44', '1', '1', '0', '2', '', '', '', 4, '-1.00', '1', 0, NULL),
(43, 6, 2, 6, 13, 9, '2016-01-03 18:48:51', '1', '1', '0', '1', '', '', '', 2, '-1.00', '1', 0, NULL),
(44, 6, 2, 6, 14, 10, '2016-01-03 18:48:57', '1', '1', '0', '4', '', '', '', 3, '3.00', '1', 0, NULL),
(45, 6, 2, 6, 1, 11, '2016-01-03 18:49:05', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(46, 6, 2, 6, 15, 12, '2016-01-03 18:49:11', '1', '1', '0', '', '', 'False', '', 1, '1.00', '1', 0, NULL),
(47, 7, 2, 4, 3, 1, '2016-01-03 21:41:10', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(48, 7, 2, 4, 6, 2, '2016-01-03 21:41:20', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(49, 7, 2, 4, 7, 3, '2016-01-03 21:41:32', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(50, 7, 2, 4, 8, 4, '2016-01-03 21:41:51', '1', '1', '0', '1', '', '', '', 2, '2.00', '1', 0, NULL),
(51, 7, 2, 4, 9, 5, '2016-01-03 21:42:07', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(52, 7, 2, 4, 10, 6, '2016-01-03 21:42:14', '1', '1', '0', '', '', '', '', 4, '-1.00', '1', 0, NULL),
(53, 7, 2, 4, 11, 7, '2016-01-03 21:42:26', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(54, 7, 2, 4, 12, 8, '2016-01-03 21:42:41', '1', '1', '0', '1', '', '', '', 4, '-1.00', '1', 0, NULL),
(55, 7, 2, 4, 13, 9, '2016-01-03 21:42:49', '1', '1', '0', '4', '', '', '', 2, '-1.00', '1', 0, NULL),
(56, 7, 2, 4, 14, 10, '2016-01-03 21:43:02', '1', '1', '0', '3', '', '', '', 3, '0.00', '1', 0, NULL),
(57, 7, 2, 4, 1, 11, '2016-01-03 21:44:07', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(58, 7, 2, 4, 15, 12, '2016-01-03 21:44:20', '1', '1', '0', '', '', 'True', '', 1, '0.00', '1', 0, NULL),
(59, 8, 5, 4, 2, 1, '2016-01-03 21:46:11', '1', '1', '0', '2', '', '', '', 4, '4.00', '1', 0, NULL),
(60, 8, 5, 4, 6, 2, '2016-01-03 21:46:20', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(61, 8, 5, 4, 5, 3, '2016-01-03 21:46:26', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(62, 8, 5, 4, 7, 4, '2016-01-03 21:46:33', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(63, 8, 5, 4, 9, 5, '2016-01-03 21:46:38', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(64, 9, 5, 5, 9, 1, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(65, 9, 5, 5, 6, 2, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(66, 9, 5, 5, 14, 3, NULL, '0', '0', '0', '', '', '', '', 3, '0.00', '1', 0, NULL),
(67, 9, 5, 5, 8, 4, NULL, '0', '0', '0', '', '', '', '', 2, '0.00', '1', 0, NULL),
(68, 9, 5, 5, 12, 5, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(69, 10, 5, 4, 10, 1, '2016-01-05 23:44:33', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(70, 10, 5, 4, 13, 2, '2016-01-05 23:44:38', '1', '1', '0', '2', '', '', '', 2, '2.00', '1', 0, NULL),
(71, 10, 5, 4, 12, 3, '2016-01-05 23:44:44', '1', '1', '0', '1', '', '', '', 4, '-1.00', '1', 0, NULL),
(72, 10, 5, 4, 11, 4, '2016-01-05 23:44:50', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(73, 10, 5, 4, 8, 5, NULL, '1', '0', '0', '', '', '', '', 2, '0.00', '1', 0, NULL),
(74, 11, 5, 4, 4, 1, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(75, 11, 5, 4, 13, 2, NULL, '0', '0', '0', '', '', '', '', 2, '0.00', '1', 0, NULL),
(76, 11, 5, 4, 8, 3, NULL, '0', '0', '0', '', '', '', '', 2, '0.00', '1', 0, NULL),
(77, 11, 5, 4, 7, 4, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(78, 11, 5, 4, 14, 5, NULL, '0', '0', '0', '', '', '', '', 3, '0.00', '1', 0, NULL),
(79, 12, 5, 5, 6, 1, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(80, 12, 5, 5, 10, 2, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(81, 12, 5, 5, 5, 3, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(82, 12, 5, 5, 12, 4, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(83, 12, 5, 5, 11, 5, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(84, 13, 5, 1, 9, 1, '2016-02-04 00:30:44', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(85, 13, 5, 1, 10, 2, '2016-02-04 00:30:49', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(86, 13, 5, 1, 7, 3, '2016-02-04 00:30:54', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(87, 13, 5, 1, 6, 4, '2016-02-04 00:31:00', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(88, 13, 5, 1, 8, 5, '2016-02-04 00:31:13', '1', '1', '0', '3', '', '', '', 2, '0.00', '1', 0, NULL),
(89, 14, 5, 1, 6, 1, '2016-02-04 00:41:11', '1', '1', '0', '1', '', '', '', 4, '0.00', '1', 0, NULL),
(90, 14, 5, 1, 9, 2, '2016-02-04 00:41:17', '1', '1', '0', '3', '', '', '', 4, '0.00', '1', 0, NULL),
(91, 14, 5, 1, 2, 3, '2016-02-04 00:41:22', '1', '1', '0', '3', '', '', '', 4, '-1.00', '1', 0, NULL),
(92, 14, 5, 1, 10, 4, '2016-02-04 00:41:30', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(93, 14, 5, 1, 12, 5, '2016-02-04 00:41:38', '1', '1', '0', '1', '', '', '', 4, '-1.00', '1', 0, NULL),
(94, 15, 5, 1, 6, 1, '2016-02-04 11:28:38', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(95, 15, 5, 1, 2, 2, '2016-02-04 11:28:45', '1', '1', '0', '2', '', '', '', 4, '4.00', '1', 0, NULL),
(96, 15, 5, 1, 14, 3, '2016-02-04 11:28:51', '1', '1', '0', '2', '', '', '', 3, '0.00', '1', 0, NULL),
(97, 15, 5, 1, 5, 4, '2016-02-04 11:28:59', '1', '1', '0', '2', '', '', '', 4, '4.00', '1', 0, NULL),
(98, 15, 5, 1, 7, 5, '2016-02-04 11:29:06', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(99, 16, 6, 1, 6, 1, '2016-02-04 11:46:53', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(100, 16, 6, 1, 7, 2, '2016-02-04 11:47:47', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(101, 16, 6, 1, 8, 3, '2016-02-04 11:47:23', '1', '1', '0', '2', '', '', '', 2, '0.00', '1', 0, NULL),
(102, 16, 6, 1, 9, 4, '2016-02-04 11:47:30', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(103, 16, 6, 1, 10, 5, '2016-02-04 11:47:39', '1', '1', '0', '1', '', '', '', 4, '0.00', '1', 0, NULL),
(104, 16, 6, 1, 11, 6, '2016-02-04 11:48:02', '1', '1', '0', '3', '', '', '', 4, '0.00', '1', 0, NULL),
(105, 16, 6, 1, 12, 7, '2016-02-04 11:48:10', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(106, 16, 6, 1, 13, 8, '2016-02-04 11:48:49', '1', '1', '0', '2', '', '', '', 2, '2.00', '1', 0, NULL),
(107, 16, 6, 1, 14, 9, '2016-02-04 11:48:56', '1', '1', '0', '3', '', '', '', 3, '0.00', '1', 0, NULL),
(108, 17, 7, 1, 9, 1, '2016-02-04 11:55:43', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(109, 17, 7, 1, 11, 2, '2016-02-04 11:55:49', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(110, 17, 7, 1, 6, 3, '2016-02-04 11:55:55', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(111, 17, 7, 1, 7, 4, '2016-02-04 11:56:02', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(112, 17, 7, 1, 2, 5, '2016-02-04 11:56:09', '1', '1', '0', '2', '', '', '', 4, '4.00', '1', 0, NULL),
(113, 17, 7, 1, 5, 6, '2016-02-04 11:56:28', '1', '1', '0', '2', '', '', '', 4, '4.00', '1', 0, NULL),
(114, 17, 7, 1, 8, 7, '2016-02-04 11:56:34', '1', '1', '0', '2', '', '', '', 2, '0.00', '1', 0, NULL),
(115, 18, 7, 1, 2, 1, '2016-02-04 12:00:30', '1', '1', '0', '4', '', '', '', 4, '-1.00', '1', 0, NULL),
(116, 18, 7, 1, 13, 2, '2016-02-04 12:00:35', '1', '1', '0', '4', '', '', '', 2, '-1.00', '1', 0, NULL),
(117, 18, 7, 1, 6, 3, '2016-02-04 12:00:41', '1', '1', '0', '4', '', '', '', 4, '0.00', '1', 0, NULL),
(118, 18, 7, 1, 10, 4, '2016-02-04 12:00:46', '1', '1', '0', '5', '', '', '', 4, '-1.00', '1', 0, NULL),
(119, 18, 7, 1, 8, 5, '2016-02-04 12:00:52', '1', '1', '0', '3', '', '', '', 2, '0.00', '1', 0, NULL),
(120, 18, 7, 1, 4, 6, '2016-02-04 12:00:57', '1', '1', '0', '4', '', '', '', 4, '4.00', '1', 0, NULL),
(121, 18, 7, 1, 11, 7, '2016-02-04 12:01:05', '1', '1', '0', '3', '', '', '', 4, '0.00', '1', 0, NULL),
(122, 19, 8, 1, 7, 1, '2016-02-04 12:12:35', '1', '1', '0', '1', '', '', '', 4, '4.00', '1', 0, NULL),
(123, 20, 8, 1, 12, 1, '2016-02-04 12:13:17', '1', '1', '0', '4', '', '', '', 4, '-1.00', '1', 0, NULL),
(124, 21, 7, 4, 14, 1, NULL, '1', '0', '0', '', '', '', '', 3, '0.00', '1', 0, NULL),
(125, 21, 7, 4, 10, 2, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(126, 21, 7, 4, 9, 3, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(127, 21, 7, 4, 11, 4, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(128, 21, 7, 4, 12, 5, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(129, 21, 7, 4, 4, 6, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(130, 21, 7, 4, 2, 7, NULL, '0', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL),
(131, 22, 9, 4, 16, 1, '2016-03-02 14:13:17', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(132, 22, 9, 4, 11, 2, '2016-03-02 14:13:23', '1', '1', '0', '2', '', '', '', 4, '0.00', '1', 0, NULL),
(133, 22, 9, 4, 12, 3, '2016-03-02 14:13:27', '1', '1', '0', '3', '', '', '', 4, '4.00', '1', 0, NULL),
(134, 22, 9, 4, 13, 4, '2016-03-02 14:13:59', '1', '1', '0', '1', '', '', '', 2, '0.00', '1', 0, NULL),
(135, 22, 9, 4, 14, 5, NULL, '1', '0', '0', '', '', '', '', 3, '0.00', '1', 0, NULL),
(136, 22, 9, 4, 15, 6, '2016-03-02 14:14:15', '1', '1', '0', '', '', 'True', '', 1, '0.00', '1', 0, NULL),
(137, 23, 8, 4, 6, 1, NULL, '1', '0', '0', '', '', '', '', 4, '0.00', '1', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `created`, `modified`) VALUES
(1, 'IIT', '2014-12-19 14:26:32', '2014-12-19 14:26:32'),
(2, 'CPMT', '2014-12-19 14:26:38', '2014-12-19 14:26:38'),
(3, 'park', '2016-01-02 13:28:59', '2016-01-02 13:28:59');

-- --------------------------------------------------------

--
-- Table structure for table `helpcontents`
--

CREATE TABLE IF NOT EXISTS `helpcontents` (
`id` int(11) NOT NULL,
  `link_title` varchar(255) NOT NULL,
  `link_desc` longtext NOT NULL,
  `status` varchar(8) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `helpcontents`
--

INSERT INTO `helpcontents` (`id`, `link_title`, `link_desc`, `status`, `created`, `modified`) VALUES
(1, 'Reference Link', '<p>IndiaBix</p>', 'Active', '2014-12-19 14:45:19', '2014-12-19 14:45:19'),
(2, 'm4maths', '<p> Visit <a href="http://m4maths.com/">m4maths.com</a></p>', 'Active', '2014-12-19 14:45:43', '2014-12-19 14:45:43');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE IF NOT EXISTS `mails` (
`id` int(11) NOT NULL,
  `to_email` varchar(100) NOT NULL,
  `from_email` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `date` datetime NOT NULL,
  `status` varchar(5) NOT NULL DEFAULT 'Live',
  `type` varchar(10) NOT NULL DEFAULT 'Unread',
  `mail_type` varchar(4) NOT NULL DEFAULT 'To'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `to_email`, `from_email`, `email`, `subject`, `message`, `date`, `status`, `type`, `mail_type`) VALUES
(1, 'student@student.com', 'Administrator', 'student@student.com', 'Demo Subject', 'Demo Content', '2015-02-24 18:33:49', 'Live', 'Read', 'To'),
(2, 'student@student.com', 'Administrator', 'Administrator', 'Demo Subject', 'Demo Content', '2015-02-24 18:33:49', 'Live', 'Read', 'From'),
(3, 'Administrator', 'student@student.com', 'Administrator', 'Demo Subject', 'Demo Content User', '2015-02-24 18:34:49', 'Live', 'Read', 'To'),
(4, 'Administrator', 'student@student.com', 'student@student.com', 'Demo Subject', 'Demo Content User', '2015-02-24 18:34:49', 'Live', 'Read', 'From'),
(5, 'Administrator', 'student@student.com', 'Administrator', 'to dear ADMIN', 'hi admin', '2016-01-01 18:02:52', 'Live', 'Read', 'To'),
(6, 'Administrator', 'student@student.com', 'student@student.com', 'to dear ADMIN', 'hi admin', '2016-01-01 18:02:52', 'Live', 'Read', 'From'),
(7, 'pradnya@abc.com', 'Administrator', 'pradnya@abc.com', 'tp', '<p>hey holla</p>', '2016-01-05 15:08:02', 'Live', 'Read', 'To'),
(8, 'pradnya@abc.com', 'Administrator', 'Administrator', 'tp', '<p>hey holla</p>', '2016-01-05 15:08:02', 'Live', 'Read', 'From'),
(9, 'Administrator', 'anujshah.70@yahoo.com', 'Administrator', 'Mygraph', '<p>Performance is reduced</p>', '2016-02-01 23:28:19', 'Live', 'Read', 'To'),
(10, 'Administrator', 'anujshah.70@yahoo.com', 'anujshah.70@yahoo.com', 'Mygraph', '<p>Performance is reduced</p>', '2016-02-01 23:28:19', 'Live', 'Read', 'From'),
(11, 'anujshah.70@yahoo.com', 'Administrator', 'anujshah.70@yahoo.com', 'hi', 'hello this is demo content', '2016-02-03 01:47:18', 'Live', 'Read', 'To'),
(12, 'anujshah.70@yahoo.com', 'Administrator', 'Administrator', 'hi', 'hello this is demo content', '2016-02-03 01:47:18', 'Live', 'Read', 'From'),
(13, 'Administrator', 'anujshah.70@yahoo.com', 'Administrator', 'yes me', 'hhfrsyuhjtkh', '2016-03-11 15:14:05', 'Live', 'Read', 'To'),
(14, 'Administrator', 'anujshah.70@yahoo.com', 'anujshah.70@yahoo.com', 'yes me', 'hhfrsyuhjtkh', '2016-03-11 15:14:05', 'Live', 'Read', 'From');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(11) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_desc` longtext NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
`id` int(11) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `controller_name` varchar(100) NOT NULL,
  `action_name` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `parent_id` int(1) NOT NULL,
  `ordering` int(11) NOT NULL,
  `sel_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `model_name`, `page_name`, `controller_name`, `action_name`, `icon`, `parent_id`, `ordering`, `sel_name`) VALUES
(1, 'Dashboard', 'Dashboard', 'admin', '', 'fa fa-dashboard', 0, 1, NULL),
(2, 'Subjects', 'Subjects', 'Subjects', 'index', 'fa fa-book', 0, 4, 'Iequestions'),
(3, 'Subjects', 'Add Question', 'Addquestions', 'add', '', 2, 3, NULL),
(4, 'Students', 'Students', 'Students', 'index', 'fa fa-graduation-cap', 0, 10, NULL),
(5, 'Exams', 'Exams', 'Exams', 'index', 'fa fa-list-alt', 0, 6, 'Attemptedpapers,Addquestions'),
(6, 'Exams', 'Attempted Papers', 'Attemptedpapers', 'index', '', 5, 6, NULL),
(7, 'Results', 'Results', 'Results', 'index', 'fa fa-trophy', 0, 7, NULL),
(10, 'Users', 'Users', 'Users', 'index', 'fa fa-user', 0, 3, NULL),
(11, 'Groups', 'Groups', 'Groups', 'index', 'fa fa-users', 0, 2, NULL),
(13, 'Contents', 'Slides', 'Slides', 'index', '', 12, 99, NULL),
(14, 'Contents', 'Organisation Logo', 'Contents', 'index', '', 12, 99, NULL),
(15, 'Contents', 'News Content', 'News', 'index', '', 12, 99, NULL),
(17, 'Contents', 'Help Content', 'Helpcontent', 'index', '', 12, 99, NULL),
(18, 'Questions', 'Questions', 'Questions', 'index', 'fa fa-question', 0, 5, NULL),
(19, 'Questions', 'Import Export', 'Iequestions', 'index', '', 18, 99, NULL),
(20, 'Payments', 'Paypal Payment', 'Payments', 'index', '', 8, 99, NULL),
(21, 'Mails', 'Mails', 'Mails', 'index', 'fa fa-envelope', 0, 8, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_rights`
--

CREATE TABLE IF NOT EXISTS `page_rights` (
`id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `ugroup_id` int(11) NOT NULL,
  `save_right` int(1) NOT NULL,
  `update_right` int(1) NOT NULL,
  `view_right` int(1) NOT NULL,
  `search_right` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_rights`
--

INSERT INTO `page_rights` (`id`, `page_id`, `ugroup_id`, `save_right`, `update_right`, `view_right`, `search_right`) VALUES
(22, 1, 2, 0, 0, 1, 0),
(23, 6, 2, 0, 0, 1, 0),
(24, 5, 2, 0, 0, 1, 0),
(26, 18, 2, 0, 0, 1, 0),
(27, 19, 2, 0, 0, 1, 0),
(28, 7, 2, 0, 0, 1, 0),
(29, 4, 2, 0, 0, 1, 0),
(30, 3, 2, 0, 0, 1, 0),
(31, 2, 2, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `qtypes`
--

CREATE TABLE IF NOT EXISTS `qtypes` (
`id` int(11) NOT NULL,
  `question_type` varchar(20) NOT NULL,
  `type` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qtypes`
--

INSERT INTO `qtypes` (`id`, `question_type`, `type`) VALUES
(1, 'Objective Questions', 'M'),
(2, 'True / False', 'T'),
(3, 'Fill in the blanks', 'F'),
(4, 'Subjective', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
`id` int(11) NOT NULL,
  `qtype_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `diff_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `option1` text NOT NULL,
  `option2` text NOT NULL,
  `option3` text NOT NULL,
  `option4` text NOT NULL,
  `option5` text NOT NULL,
  `option6` text NOT NULL,
  `marks` float NOT NULL,
  `negative_marks` float NOT NULL,
  `hint` text NOT NULL,
  `explanation` text NOT NULL,
  `answer` varchar(15) NOT NULL,
  `true_false` varchar(5) NOT NULL,
  `fill_blank` varchar(100) NOT NULL,
  `status` varchar(3) NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `qtype_id`, `subject_id`, `diff_id`, `question`, `option1`, `option2`, `option3`, `option4`, `option5`, `option6`, `marks`, `negative_marks`, `hint`, `explanation`, `answer`, `true_false`, `fill_blank`, `status`) VALUES
(1, 1, 1, 3, 'If six identical cells each having an emf of 6 V are connected in parallel, the emf of the combination is', '1V', '1/6 V', '36V', '6V', '', '', 4, 4, '', '', '4', '', '', 'Yes'),
(2, 1, 3, 2, 'The points (3a, 0), (0, 3b) and (a, 2b) are.', 'Vertices of an equilateral triangle', 'Collinear', 'Vertices of an isosceles triangle', 'Vertices of a right angled isosceles triangle', '', '', 4, 4, '', '', '2', '', '', 'Yes'),
(3, 1, 2, 1, ' In a solid lattice the cation has left a lattice site and is located at an interstitial position, the lattice defect is', 'Interstitial defect', 'Valency defect', 'Frenkel defect', 'Schottky defect', '', '', 4, 3, '', '', '3', '', '', 'Yes'),
(4, 1, 3, 3, 'If six identical cells each having an emf of 6 V are connected in parallel, the emf of the combination is', '1V', '1/6 V', '36V', '6V', '', '', 4, 3, '', '', '4', '', '', 'Yes'),
(5, 1, 3, 2, 'The points (3a, 0), (0, 3b) and (a, 2b) are.', 'Vertices of an equilateral triangle', 'Collinear', 'Vertices of an isosceles triangle', 'Vertices of a right angled isosceles triangle', '', '', 4, 1, '', '', '2', '', '', 'Yes'),
(6, 1, 3, 1, ' In a solid lattice the cation has left a lattice site and is located at an interstitial position, the lattice defect is', 'Interstitial defect', 'Valency defect', 'Frenkel defect', 'Schottky defect', '', '', 4, 0, '', '', '3', '', '', 'Yes'),
(7, 1, 3, 1, 'Identify the order1. The web browser requests a webpage using HTTP.', '4,2,1,3', '1,2,3,4', '4,1,2,3', '2,4,1,3', '', '', 4, 1, '', '', '1', '', '', 'Yes'),
(8, 1, 3, 2, 'Assume that there are 3 page frames which are initially empty. If the page reference string 1,', '7', '10', '8', '9', '6', '', 2, 2, '', '', '1', '', '', 'Yes'),
(9, 1, 3, 3, 'Father is aged three times more than his son Ronit. After 8 years, he would be two and a half times of Ronit''s age. After further 8 years, how many times would he be of Ronit''s age?', '2 TIMES', '2.5 TIMES', '3 TIMES', '4 TIMES', '', '', 4, 0, '', '', '1', '', '', 'Yes'),
(10, 1, 3, 1, 'A is two years older than B who is twice as old as C. If the total of the ages of A, B and C be 27, the how old is B?', '7', '8', '9', '10', '11', '', 4, 7, '', '', '4', '', '', 'Yes'),
(11, 1, 3, 1, 'A man is 24 years older than his son. In two years, his age will be twice the age of his son. The present age of his son is:', '14', '18', '20', '22', '', '', 4, 0, '', '', '4', '', '', 'Yes'),
(12, 1, 3, 2, 'Six years ago, the ratio of the ages of Kunal and Sagar was 6 : 5. Four years hence, the ratio of their ages will be 11 : 10. What is Sagar''s age at present?', '18', '20', '16', '19', '', '', 4, 1, '', '', '3', '', '', 'Yes'),
(13, 1, 3, 3, 'At present, the ratio between the ages of Arun and Deepak is 4 : 3. After 6 years, Arun''s age will be 26 years. What is the age of Deepak at present ?', '12', '15', '19', '21', '', '', 2, 1, '', '', '2', '', '', 'Yes'),
(14, 1, 3, 3, 'Sachin is younger than Rahul by 7 years. If their ages are in the respective ratio of 7 : 9, how old is Sachin?', '12', '18', '28', '28.5', '', '', 3, 2, '', '', '4', '', '', 'Yes'),
(15, 2, 1, 1, 'jhgdhgkfhvskdcnkjh', 'jhzxjkjjnkl', 'kljlk', 'kjijolk', '', '', '', 1, 3, '', 'jhkhk', '', 'False', '', 'Yes'),
(16, 1, 2, 3, '<p>&nbsp;</p>\r\n<p><img src="/exam/img/Uploads/20160110_152125.jpg" alt="chidya" width="300" height="300" /></p>\r\n<p>name the bird?</p>', '<p>chidya</p>', '<p>crow</p>', '<p>riddhi</p>', '<p>peacock</p>', '<p><img src="/exam/img/Uploads/20160110_152125.jpg" alt="" width="300" height="300" /></p>', '', 4, 0, 'male', '<p><img src="/exam/img/Uploads/IMG_20160113_115904815.jpg" alt="" width="300" height="300" /></p>', '3', '', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `question_groups`
--

CREATE TABLE IF NOT EXISTS `question_groups` (
`id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_groups`
--

INSERT INTO `question_groups` (`id`, `question_id`, `group_id`) VALUES
(8, 5, 2),
(9, 5, 1),
(10, 5, 3),
(11, 6, 2),
(12, 6, 1),
(13, 6, 3),
(14, 7, 2),
(15, 7, 1),
(16, 7, 3),
(20, 9, 2),
(21, 9, 1),
(22, 9, 3),
(26, 11, 2),
(27, 11, 1),
(28, 11, 3),
(29, 12, 2),
(30, 12, 1),
(31, 12, 3),
(32, 13, 2),
(33, 13, 1),
(34, 13, 3),
(47, 1, 2),
(48, 1, 1),
(49, 1, 3),
(51, 2, 2),
(52, 2, 1),
(53, 2, 3),
(54, 3, 2),
(55, 3, 1),
(56, 3, 3),
(57, 4, 2),
(58, 4, 1),
(59, 4, 3),
(60, 10, 2),
(61, 10, 1),
(62, 10, 3),
(63, 15, 2),
(64, 15, 1),
(65, 15, 3),
(66, 14, 2),
(67, 14, 1),
(68, 14, 3),
(69, 8, 2),
(70, 8, 1),
(71, 8, 3),
(72, 16, 2),
(73, 16, 1),
(74, 16, 3);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
`id` int(11) NOT NULL,
  `slide_name` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `slide_name`, `ordering`, `photo`, `dir`, `status`, `created`, `modified`) VALUES
(1, 'Slide1', 1, '54acf756c29571558ba3b9e80cbdffb0.jpg', '', 'Suspend', '2014-12-19 14:42:37', '2016-01-02 15:49:04'),
(2, 'Slide2', 2, 'f403bd72abe789c270e90144753c79de.jpg', '', 'Suspend', '2014-12-19 14:43:03', '2016-01-02 15:48:42'),
(3, 'Slide3', 3, '9f1fc418524c437f6803b0d7c8ff4fc2.jpg', '', 'Active', '2014-12-19 14:43:27', '2014-12-19 14:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
`id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `guardian_phone` varchar(15) NOT NULL,
  `enroll` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `status` varchar(7) NOT NULL DEFAULT 'Pending',
  `reg_code` varchar(6) NOT NULL,
  `reg_status` varchar(4) NOT NULL DEFAULT 'Live',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `email`, `password`, `name`, `address`, `phone`, `guardian_phone`, `enroll`, `photo`, `dir`, `status`, `reg_code`, `reg_status`, `created`, `modified`, `last_login`) VALUES
(1, 'student@student.com', '4768db88022aa663e9945507b0d20c144e7b09ead4744f3c3c9889734211eb42', 'Demo 1', 'Demo Address', '12345679890', '', 'I-9887457744340', '', '', 'Active', '', 'Done', '2014-12-19 14:39:13', '2016-03-01 17:58:54', '0000-00-00 00:00:00'),
(2, 'lmn@lmn.com', 'cb1bc71406c9087414ad1c8d74ae2956b2c492cd153c63b3cfe7aab506e541bf', 'lmn123', 'qewrty123ii', '7894561230', '13456', '', '', '', 'Active', '', 'Done', '2016-01-02 13:30:09', '2016-03-01 17:59:10', '0000-00-00 00:00:00'),
(3, 'riddhichheda03@gmail.com', '3564f9e27345111bdd6db013980eb1e55d2effd261e6f18fd50c2ef791497a63', ' riddhi', 'dvsvdsfv', '7894651230', '', '', '', '', 'Active', '', 'Done', '2016-01-02 13:41:52', '2016-03-01 17:59:22', '0000-00-00 00:00:00'),
(4, 'anujshah.70@yahoo.com', 'bb7e2b214be10206920955b32d99a7edd903dad3add28404c6d6014064036829', 'anuj shah', 'add', '1234567890', '', '101', '62b349d42c8b2b59397ca8d4a755befd.jpg', '', 'Active', '', 'Done', '2016-01-02 16:58:22', '2016-03-02 13:55:25', '0000-00-00 00:00:00'),
(5, 'pradnya@abc.com', '53b4a01e06bf514a2275eddb0154529a9be902d8a40692188c00092e8ca7046f', 'pradnya', 'truhfjkjcmkksll7999', '789456123023', '', '', '050943d70cb32deab560932a1422720d.png', '', 'Active', '', 'Done', '2016-01-02 17:04:54', '2016-01-05 12:37:19', '0000-00-00 00:00:00'),
(6, 'khushali@khushali.com', '6e331a138b0da7efb3269625ac721c1d0fc7db57f6c628b20f1723ee22c57218', 'khushali', 'ajdj', '1111111111', '', '', '', '', 'Active', '', 'Done', '2016-01-03 18:45:12', '2016-01-03 18:46:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_groups`
--

CREATE TABLE IF NOT EXISTS `student_groups` (
`id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_groups`
--

INSERT INTO `student_groups` (`id`, `student_id`, `group_id`) VALUES
(26, 1, 1),
(27, 2, 1),
(28, 3, 2),
(29, 4, 2),
(12, 5, 3),
(14, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
`id` int(11) NOT NULL,
  `subject_name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `created`, `modified`) VALUES
(1, 'Physics', '2014-12-19 14:28:33', '2014-12-19 14:28:33'),
(2, 'Chemistry', '2014-12-19 14:28:41', '2014-12-19 14:28:41'),
(3, 'Maths', '2014-12-19 14:28:50', '2014-12-19 14:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `subject_groups`
--

CREATE TABLE IF NOT EXISTS `subject_groups` (
`id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_groups`
--

INSERT INTO `subject_groups` (`id`, `subject_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 2, 1),
(4, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ugroups`
--

CREATE TABLE IF NOT EXISTS `ugroups` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ugroups`
--

INSERT INTO `ugroups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrator', '2012-07-05 17:16:24', '2012-07-05 17:16:24'),
(2, 'Instructor', '2014-12-12 12:03:23', '2014-12-12 12:03:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `mobile` varchar(10) NOT NULL,
  `ugroup_id` int(11) NOT NULL DEFAULT '2',
  `status` enum('Active','Suspend') DEFAULT 'Active',
  `deleted` char(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `name`, `mobile`, `ugroup_id`, `status`, `deleted`, `created`, `modified`) VALUES
(1, 'admin', 'dfb37faf99ffd691383e054541f1a3fd1966273d359d85aa419562fc26bf4427', 'admin@gmail.com', 'Administrator', '9839512921', 1, 'Active', NULL, '2014-04-01 21:08:06', '2014-12-19 12:17:07'),
(2, 'nishi', '599afea7693edce101bbf4f2557b3fb0105b87e84897c1c41003c1a90df4f996', 'nishinikunj@vishwa.com', 'nishi', '1234567890', 2, 'Active', NULL, '2016-01-02 17:25:03', '2016-01-02 17:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_data`
--

CREATE TABLE IF NOT EXISTS `user_data` (
`uid` int(11) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(240) NOT NULL,
  `avtarpath` varchar(400) NOT NULL DEFAULT 'http://localhost/signup/images/avtar.png'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_data`
--

INSERT INTO `user_data` (`uid`, `fname`, `email`, `password`, `avtarpath`) VALUES
(4, 'riddhi', 'riddhi@chheda.com', 'b5b00d3e7a2756c936a0118124032818', 'images/14-03-2016-1457978671.jpg'),
(8, 'anuj', 'anuj.shah70@yahoo.com', 'c482e3014f9b268c6d953a0fb0df6cc6', 'images/15-03-2016-1458054585.jpg'),
(9, 'tanvi', 'anujshah.2016@gmail.com', 'c482e3014f9b268c6d953a0fb0df6cc6', 'http://localhost/signup/images/avtar.png'),
(10, 'anuj', 'anujshah.scientist@gmail.com', 'c482e3014f9b268c6d953a0fb0df6cc6', 'http://localhost/signup/images/avtar.png');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 2),
(5, 2, 1),
(6, 2, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `diffs`
--
ALTER TABLE `diffs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_groups`
--
ALTER TABLE `exam_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `exam_id` (`exam_id`), ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `exam_orders`
--
ALTER TABLE `exam_orders`
 ADD PRIMARY KEY (`id`), ADD KEY `exam_id` (`exam_id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `exam_preps`
--
ALTER TABLE `exam_preps`
 ADD PRIMARY KEY (`id`), ADD KEY `exam_id` (`exam_id`), ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `exam_questions`
--
ALTER TABLE `exam_questions`
 ADD PRIMARY KEY (`id`), ADD KEY `exam_id` (`exam_id`), ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `exam_results`
--
ALTER TABLE `exam_results`
 ADD PRIMARY KEY (`id`), ADD KEY `exam_id` (`exam_id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `exam_stats`
--
ALTER TABLE `exam_stats`
 ADD PRIMARY KEY (`id`), ADD KEY `exam_id` (`exam_id`), ADD KEY `student_id` (`student_id`), ADD KEY `question_id` (`question_id`), ADD KEY `exam_result_id` (`exam_result_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `group_name` (`group_name`);

--
-- Indexes for table `helpcontents`
--
ALTER TABLE `helpcontents`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_rights`
--
ALTER TABLE `page_rights`
 ADD PRIMARY KEY (`id`), ADD KEY `page_id` (`page_id`), ADD KEY `ugroup_id` (`ugroup_id`);

--
-- Indexes for table `qtypes`
--
ALTER TABLE `qtypes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
 ADD PRIMARY KEY (`id`), ADD KEY `qtype_id` (`qtype_id`), ADD KEY `subject_id` (`subject_id`), ADD KEY `diff_id` (`diff_id`);

--
-- Indexes for table `question_groups`
--
ALTER TABLE `question_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `question_id` (`question_id`), ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `student_groups`
--
ALTER TABLE `student_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `student_id` (`student_id`,`group_id`), ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `subject_name` (`subject_name`);

--
-- Indexes for table `subject_groups`
--
ALTER TABLE `subject_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `subject_id` (`subject_id`), ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `ugroups`
--
ALTER TABLE `ugroups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD KEY `ugroup_id` (`ugroup_id`);

--
-- Indexes for table `user_data`
--
ALTER TABLE `user_data`
 ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `group_id` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `diffs`
--
ALTER TABLE `diffs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `exam_groups`
--
ALTER TABLE `exam_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `exam_orders`
--
ALTER TABLE `exam_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exam_preps`
--
ALTER TABLE `exam_preps`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `exam_questions`
--
ALTER TABLE `exam_questions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `exam_results`
--
ALTER TABLE `exam_results`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `exam_stats`
--
ALTER TABLE `exam_stats`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `helpcontents`
--
ALTER TABLE `helpcontents`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `page_rights`
--
ALTER TABLE `page_rights`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `qtypes`
--
ALTER TABLE `qtypes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `question_groups`
--
ALTER TABLE `question_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `student_groups`
--
ALTER TABLE `student_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subject_groups`
--
ALTER TABLE `subject_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ugroups`
--
ALTER TABLE `ugroups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_data`
--
ALTER TABLE `user_data`
MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `exam_groups`
--
ALTER TABLE `exam_groups`
ADD CONSTRAINT `exam_groups_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_orders`
--
ALTER TABLE `exam_orders`
ADD CONSTRAINT `exam_orders_ibfk_2` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_orders_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_preps`
--
ALTER TABLE `exam_preps`
ADD CONSTRAINT `exam_preps_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_preps_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `exam_questions`
--
ALTER TABLE `exam_questions`
ADD CONSTRAINT `exam_questions_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_questions_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_results`
--
ALTER TABLE `exam_results`
ADD CONSTRAINT `exam_results_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_results_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_stats`
--
ALTER TABLE `exam_stats`
ADD CONSTRAINT `exam_stats_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_stats_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `exam_stats_ibfk_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
ADD CONSTRAINT `exam_stats_ibfk_4` FOREIGN KEY (`exam_result_id`) REFERENCES `exam_results` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_rights`
--
ALTER TABLE `page_rights`
ADD CONSTRAINT `page_rights_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `page_rights_ibfk_2` FOREIGN KEY (`ugroup_id`) REFERENCES `ugroups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
ADD CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `questions_ibfk_3` FOREIGN KEY (`qtype_id`) REFERENCES `qtypes` (`id`),
ADD CONSTRAINT `questions_ibfk_4` FOREIGN KEY (`diff_id`) REFERENCES `diffs` (`id`);

--
-- Constraints for table `question_groups`
--
ALTER TABLE `question_groups`
ADD CONSTRAINT `question_groups_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `question_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_groups`
--
ALTER TABLE `student_groups`
ADD CONSTRAINT `student_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `student_groups_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subject_groups`
--
ALTER TABLE `subject_groups`
ADD CONSTRAINT `subject_groups_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `subject_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ugroup_id`) REFERENCES `ugroups` (`id`);

--
-- Constraints for table `user_groups`
--
ALTER TABLE `user_groups`
ADD CONSTRAINT `user_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `user_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
