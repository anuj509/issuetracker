<?php
session_start();
include_once 'database.php';

if(!isset($_SESSION['user']))
{
 header("Location: index.php");
}
$res=mysql_query("SELECT * FROM user_data WHERE uid=".$_SESSION['user']);

$userRow=mysql_fetch_array($res);
//image insertion

?>
<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="http://snipplicious.com/css/bootstrap-3.2.0.min.css">

<link rel="stylesheet" type="text/css" href="http://snipplicious.com/css/font-awesome-4.1.0.min.css">

<script src="http://snipplicious.com/js/jquery.js"></script>
<script src="http://snipplicious.com/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container" style="padding-top: 0px;">
  <h1 class="page-header">Edit Profile</h1>
  <div class="row">
    <!-- left column -->
	<form id ="form" action="insert.php" method="post" enctype="multipart/form-data" >
	
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="text-center">
        <img src="<?php echo $userRow['avtarpath']; ?>" class="avatar img-circle img-thumbnail" alt="avatar" />
        <h6>Upload a different photo...</h6>
        <input type="file" class="text-center center-block well well-sm" name="uploadedimage">
	  <input class="btn btn-primary" value="Save Profile" type="submit">
	  </div>
    </div>
	</form>
	
    <!-- edit form column -->
    <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
      <h3>Personal info</h3>
      <form class="form-horizontal" role="form" action="p_update.php" method="post" >
        <div class="form-group">
          <label class="col-lg-3 control-label">Email:</label>
          <div class="col-lg-8">
            <input id="email" name="email" class="form-control" value="<?php echo $userRow['email']; ?>" type="text" required>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-md-3 control-label">Username:</label>
          <div class="col-md-8">
            <input id="fname" name="fname" class="form-control" value="<?php echo $userRow['fname']; ?>" type="text" required >
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Password:</label>
          <div class="col-md-8">
            <input id="pass" name="pass" class="form-control" value="" type="password" required>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Confirm password:</label>
          <div class="col-md-8">
            <input id="c_pass" name="c_pass" class="form-control" value="" type="password" required>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label"></label>
          <div class="col-md-8">
            <input class="btn btn-primary" value="Save Changes" type="submit">
            <span></span>
            <input class="btn btn-default" value="Cancel" type="reset">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>